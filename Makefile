all: hello_world.cc fibonacci.cc magic_square.cc sort_int.cc
	g++ -o hello_world hello_world.cc
	g++ -o fibonacci fibonacci.cc
	g++ -o magic_square magic_square.cc
	g++ -o sort_int sort_int.cc
clean:
	rm hello_world fibonacci magic_square sort_int

