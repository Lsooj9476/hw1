#include<iostream>
using std::cout;
using std::cin;

int Fibo(int*,int*,int,int);

int main(){
	int num;
	cin>>num;
	int a=1,b=1;
	for(int i=0;i<num;i++)
		Fibo(&a,&b,i,num);
	return 0;
}

int Fibo(int *a,int *b,int n,int num){
	if(n==0 || n==1) cout<<"1 ";
	else if(n<num){ 
		cout<<*a+*b<<" ";
		int temp=*a;
		*a=*b;
		*b=temp+*a;
	}
}
