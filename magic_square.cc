#include<iostream>
#include<iomanip>

using std::cout;
using std::cin;
using std::endl;

void PrintMatrix(int*,int);
void MagicSquare(int*,int);

main(){
	int n;
	cin>>n;
	if(n<0||n%2==0) return 0;
	else{
		int *Mat=new int[n*n];
		MagicSquare(Mat,n);
		delete[] Mat;
	}
	return 0;
}

void PrintMatrix(int* Matrix,int n){
	for(int y=0;y<n;y++){
		for(int x=0;x<n;x++)
			cout<<std::left<<std::setw(5)<<Matrix[y*n+x];
		cout<<endl;
	}
}

void MagicSquare(int* MagSquare,int n){
	int y=0,x=(n-1)/2;
	MagSquare[y*n+x]=1;
	for(int i=2;i<=n*n;i++){
		if(y==0&&x==(n-1)) y++;
		else if(y==0){
			y=n-1;x++;
		}
		else if(x==(n-1)){
			y--;x=0;
		}
		else if(MagSquare[(y-1)*n+(x+1)]!=0) y++;
		else{
			y--;x++;
		}
		MagSquare[y*n+x]=i;
	}
	PrintMatrix(MagSquare,n);
}
